TEMPLATE:
https://onepagelove.com/justice

AUTHOR:
DESIGNED & DEVELOPED by FreeHTML5.co

Website: http://freehtml5.co/
Twitter: http://twitter.com/fh5co
Facebook: http://facebook.com/fh5co


CREDITS:

Bootstrap
http://getbootstrap.com/

jQuery
http://jquery.com/

jQuery Easing
http://gsgd.co.uk/sandbox/jquery/easing/

Modernizr
http://modernizr.com/

Google Fonts
https://www.google.com/fonts/

Icomoon
https://icomoon.io/app/

Respond JS
https://github.com/scottjehl/Respond/blob/master/LICENSE-MIT

animate.css
http://daneden.me/animate

jQuery Waypoint
https://github.com/imakewebthings/waypoints/blog/master/licenses.txt

Stellar Parallax
http://markdalgleish.com/projects/stellar.js/


Magnific Popup
http://dimsemenov.com/plugins/magnific-popup/

Demo Images:
http://unsplash.com

==========================================================================

Images:
https://www.pexels.com/photo/black-twitter-speaker-128611/
